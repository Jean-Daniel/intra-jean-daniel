var express    = require('express');
var app        = express();
var Sequelize = require('sequelize');
var mysql = require('mysql');
var sequelize = new Sequelize('mysql://root@localhost:3306/node');


var port     = process.env.PORT || 8080; 

//definition de table
var User = sequelize.define('user', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
  },
  nom: Sequelize.STRING,
  email: Sequelize.STRING
})

//create de user
User.sync({force: true}).then(function () {

  return User.create({
    id: 100,
    nom: 'jean-daniel',
    email: 'jean-daniel@hotmail.com'
  });
});
User.sync({force: true}).then(function () {

  return User.create({
    id: 101,
    nom: 'jean',
    email: 'jean@hotmail.com'
  });
});
User.sync({force: true}).then(function () {

  return User.create({
    id: 102,
    nom: 'daniel',
    email: 'daniel@hotmail.com'
  });
});

app.get('/', function(req,res){
	res.sendfile('views/test.html', {root: __dirname })
})

app.post('/', function(req,res){

	var retour = User.findOne({
			where: {id: req.body.id}
		});
	res.json(retour);
})


app.listen(port);
console.log('App listen on port ' + port);
